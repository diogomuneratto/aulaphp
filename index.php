<?php
    include 'private/connect.php';
    include 'header.php';
    
    if(isset($_GET["id"])){
        $id = $_GET['id'];        
        $sqlquery = "SELECT * FROM endereco WHERE id = '{$id}'";
        if ($result = mysqli_query($mysqli, $sqlquery)) {
            $row = mysqli_fetch_assoc($result);
        }
    }
?>

<div class="container">
    <div class="alert alert-dark">Buscando CEP com requisição AJAX</div>

    <form id="form" onsubmit="return false;">
        <div class="row">
            <div class="col-md-3">
                <label>CEP</label>
                <div class="input-group">
                    <input type="text" name="cep" class="form-control" placeholder="00000-000" onblur="getCep()">
                    <div class="input-group-prepend">
                        <div id="cep" class="input-group-text" onclick="getCep()">@</div>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="form-group">
                    <label>Endereço</label>
                    <input type="text" class="form-control" name="endereco" value="<?php echo (isset($row['endereco'])) ? $row['endereco'] : ''; ?>">
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>Numero</label>
                    <input type="text" class="form-control num" name="numero"  value="<?php echo (isset($row['numero'])) ? $row['numero'] : ''; ?>">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Bairro</label>
                    <input type="text" class="form-control" name="bairro">
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <label>Complemento</label>
                    <input type="text" class="form-control" name="complemento">
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <label>Cidade</label>
                    <input type="text" class="form-control cidade" name="cidade">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>Estado</label>
                    <input id="estado" type="text" class="form-control" name="estado">
                </div>
            </div>            
        </div>
        <input type="hidden" name="id">
        <button id="send" class="btn btn-success">Enviar</button>
    </form>

    <br/>
    <h2>Tabela de endereço</h2>

    <table class="table">
        <thead>
            <tr>
                <td>Endereço</td>
                <td>Cidade</td>
                <td>Estado</td>
                <td>Ação</td>
            </tr>
        </thead>
        <tbody>

            <?php
                $sqlquery = "SELECT * FROM endereco";
                if ($result = mysqli_query($mysqli, $sqlquery)) {
                    while ($row = mysqli_fetch_assoc($result)) {
                        $js = json_encode($row);
                        echo "<tr>
                                <td>{$row['endereco']}</td>
                                <td>{$row['cidade']}</td>
                                <td>{$row['estado']}</td>
                                <td>
                                    <button class='btn btn-primary btn-editar2' data='{$row['id']}'>Editar2</button>
                                    <button class='btn btn-info btn-editar' data='{$js}'>Editar</button> 
                                    <button class='btn btn-danger btn-delete' data='{$row['id']}'>Deletar</button>
                                </td>
                            </tr>";
                    }
                    mysqli_free_result($result);
                }
            ?>

            
        </tbody>
    </table>
</div>

<?php
    include 'footer.php';
?>