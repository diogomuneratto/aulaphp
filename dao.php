<?php
    include 'private/connect.php';


    if(isset($_GET['ajax']) && $_GET["ajax"]){
        
        $msg = '';
        if(isset($_POST["id"]) && $_POST["id"] == ''){
            $sql = "INSERT INTO endereco (cep, endereco, numero, bairro, complemento, cidade, estado) VALUES (?, ?, ?,?, ?, ?, ?)";
        } else {
            $sql = "UPDATE endereco SET cep = ?, endereco = ?, numero = ?, bairro = ?, complemento = ?, cidade = ?, estado = ? WHERE id = '{$_POST["id"]}'";
        }        

        $stmt = $mysqli->prepare($sql);
        if(!$stmt) {
            echo 'Error: '.$mysqli->error;
        }

        $stmt->bind_param('sssssss', 
            $_POST['cep'], 
            $_POST['endereco'], 
            $_POST['numero'], 
            $_POST['bairro'], 
            $_POST['complemento'], 
            $_POST['cidade'], 
            $_POST['estado']
        );

        $stmt->execute();

        if(isset($_POST["id"]) && $_POST["id"] == ''){
            $msg = 'Cadastro';
            $cd =  $stmt->insert_id;
        } else {
            $msg = 'Atualizado';
            $cd = $_POST["id"];
        }

        header('Content-Type: application/json');
        echo json_encode(array('type' => 'success', 'msg' => $msg.' com sucesso', 'code' => $cd));
        $stmt->close();
    }

    if(isset($_GET['delete'])){
        $sql = "DELETE FROM endereco WHERE id = '{$_GET['delete']}'";
        $stmt = $mysqli->prepare($sql);
        if(!$stmt) {
            echo 'Error: '.$mysqli->error;
        }
        $stmt->execute();
        $stmt->close();
    }

    if(isset($_GET["id"])){
        $id = $_GET['id'];        
        $sqlquery = "SELECT * FROM endereco WHERE id = '{$id}'";
        if ($result = mysqli_query($mysqli, $sqlquery)) {
            $row = mysqli_fetch_assoc($result);
            echo json_encode($row);
        }
    }
        


        


    

?>