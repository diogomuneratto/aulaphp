$("#send").on("click", function() {
    var form = $('#form');
    $.ajax({
            url: '/dao.php?ajax=true',
            type: 'POST',
            data: form.serialize(),
            beforeSend: function() {
                $(".alert").html("ENVIANDO...");
            }
        })
        .done(function(resp) {
            if (resp['type'] == 'success') {
                $('.alert').removeClass('alert-dark alert-danger').addClass('alert-success').html(resp['msg']);
                setTimeout(function() {
                    window.location.reload();
                }, 1500);
            }
        })
        .fail(function(jqXHR, textStatus, resp) {
            $('.alert').removeClass('alert-dark').addClass('alert-danger').html('Cep não encontrado.');
        });

});

function updateForm(v) {
    $('input[name=id]').val(v.id);
    $('input[name=cep]').val(v.cep);
    $('input[name=endereco]').val(v.endereco);
    $('input[name=numero]').val(v.numero);
    $('input[name=bairro]').val(v.bairro);
    $('input[name=complemento]').val(v.complemento);
    $('.cidade').val(v.cidade);
    $('#estado').val(v.estado);
}


$('.btn-editar2').click(function() {
    var id = $(this).attr('data');
    $.ajax({
        url: '/dao.php?id=' + id,
        type: 'GET'
    }).done(function(data) {
        var v = JSON.parse(data);
        updateForm(v);
    })
})

/*   Editar com Obj  */
$('.btn-editar').click(function() {
    var v = JSON.parse($(this).attr('data'));
    updateForm(v);
})

$('.btn-delete').click(function() {
    var id = $(this).attr('data');
    var name = confirm("Pressione um botão.")
    if (name == true) {
        $.ajax({
            url: '/dao.php?delete=' + id,
            type: 'GET'
        }).done(function() {
            alert('Registro deletado com sucesso.')
                //window.location.href = 'https://google.com.br';
            window.location.location();
        })
    } else {
        alert('Registro não foi deletado.');
    }

})

function setForm(v) {
    $('input[name=endereco]').val(v.logradouro);
    $('input[name=bairro]').val(v.bairro);
    $('input[name=complemento]').val(v.complemento);
    $('.cidade').val(v.localidade);
    $('#estado').val(v.uf);
}

function getCep() {

    var cep = $('input[name=cep]').val();
    if (cep.length < 7) {
        $('.alert').removeClass('alert-dark').addClass('alert-danger').html('Minimo de <b>7 digitos</b>.');
        return false
    }
    var url = 'http://viacep.com.br/ws/' + cep + '/json';

    $.ajax({
            url: url,
            type: 'get',
            beforeSend: function() {
                $(".alert").html("ENVIANDO...");
            }
        })
        .done(function(resp) {

            $('input[name=endereco]').val(resp.logradouro);
            $('input[name=bairro]').val(resp.bairro);
            $('input[name=complemento]').val(resp.complemento);
            $('.cidade').val(resp.localidade);
            $('#estado').val(resp.uf);

            $('.alert').removeClass('alert-dark alert-danger').addClass('alert-success').html('Cep encontrado com sucesso.');
            $('.num').focus();
        })
        .fail(function(jqXHR, textStatus, resp) {
            $('.alert').removeClass('alert-dark').addClass('alert-danger').html('Cep não encontrado.');
        });

}