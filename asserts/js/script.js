$("#send").on("click", function () {
    var form = $('#form');
    $.ajax({
        url: 'dao.php?ajax=true',
        type: 'POST',
        data: form.serialize(),
        beforeSend: function () {
            $(".alert").html("ENVIANDO...");
        }
    })
        .done(function (resp) {
            if (resp['type'] == 'success') {
                $('.alert').removeClass('alert-dark alert-danger').addClass('alert-success').html(resp['msg']);
                window.location.reload();
            }
        })
        .fail(function (jqXHR, textStatus, resp) {
            $('.alert').removeClass('alert-dark').addClass('alert-danger').html('Cep não encontrado.');
        });

});

$('.btn-editar').click(function () {

    var id = $(this).attr('data');
alert(id)
    $("#send").on("click", function () {
        var form = $('#form');
        $.ajax({
            url: 'dao.php?id=' + id,
            type: 'POST',
            data: form.serialize(),
            beforeSend: function () {
                $(".alert").html("ENVIANDO...");
            }
        })
            .done(function (resp) {
                if (resp['type'] == 'success') {
                    $('.alert').removeClass('alert-dark alert-danger').addClass('alert-success').html(resp['msg']);
                }
            })
            .fail(function (jqXHR, textStatus, resp) {
                $('.alert').removeClass('alert-dark').addClass('alert-danger').html('Cep não encontrado.');
            });

    });
})

$('.btn-delete').click(function () {
    var id = $(this).attr('data');
    var name = confirm("Pressione um botão")
    if (name == true) {
        $.ajax({
            url: 'dao.php?delete' + id,
            type: 'POST',
        }).done(function () {
            alert('Registro deletado com sucesso!')
            window.location.reload();
        })

    }
})

function setForm(v) {
    $('input[name=endereco]').val(v.logradouro);
    $('input[name=bairro]').val(v.bairro);
    $('input[name=complemento]').val(v.complemento);
    $('.cidade').val(v.localidade);
    $('#estado').val(v.uf);
}


function getCep() {

    var cep = $('input[name=cep]').val();
    if (cep.length < 7) {
        $('.alert').removeClass('alert-dark').addClass('alert-danger').html('Minimo de <b>7 digitos</b>.');
        return false
    }
    var url = 'http://viacep.com.br/ws/' + cep + '/json';


    $.ajax({
        url: url,
        type: 'get',
        beforeSend: function () {
            $(".alert").html("ENVIANDO...");
        }
    })
        .done(function (resp) {
            //$("#resultado").html(JSON.stringify(msg));

            $('input[name=endereco]').val(resp.logradouro);
            $('input[name=bairro]').val(resp.bairro);
            $('input[name=complemento]').val(resp.complemento);
            $('.cidade').val(resp.localidade);
            $('#estado').val(resp.uf);

            $('.alert').removeClass('alert-dark alert-danger').addClass('alert-success').html('Cep encontrado com sucesso.');
            $('.num').focus();
        })
        .fail(function (jqXHR, textStatus, resp) {
            //alert(resp);
            $('.alert').removeClass('alert-dark').addClass('alert-danger').html('Cep não encontrado.');
        });

}